let score = document.getElementById("score").innerHTML;
    score = parseInt(score);
let points = document.getElementById('points').innerHTML;

let clickValue = 1;
let multiplier = 2;
let multi2 = 3;
let multi3 = 5;
let autoClick = 0;
let bonusClick = 2; 

let multiplierCost = 5;
let multi2Cost = 5;
let multi3Cost = 5;
let autoClickCost = 5;
let autoClickCost2 = 5;
let autoClickCost3 = 5;
let bonusCost = 5;

let pinson = document.getElementById('pinson');
let moineau = document.getElementById('moineau');
let coucou = document.getElementById('coucou');
let pigeon = document.getElementById('pigeon');
let hirondelle = document.getElementById('hirondelle');
let resetButton = document.getElementById('reset');
let bonus = document.getElementById('merle');

let clickMulti = document.getElementById('nbrmulti1').innerHTML;
clickMulti = parseInt(clickMulti);
let clickMulti2 = document.getElementById('nbrmulti2').innerHTML;
clickMulti2 = parseInt(clickMulti2);
let clickMulti3 = document.getElementById('nbrmulti3').innerHTML;
clickMulti3 = parseInt(clickMulti3);

let clickAuto = document.getElementById('nbrautoclick1').innerHTML;
    clickAuto = parseInt(clickAuto);
let clickAuto2 = document.getElementById('nbrautoclick2').innerHTML;
    clickAuto2 = parseInt(clickAuto2);
let clickAuto3 = document.getElementById('nbrautoclick3').innerHTML;
    clickAuto3 = parseInt(clickAuto3);

resetButton = location.reload

function update(){
  document.getElementById('score').innerHTML = score;
  document.getElementById('nbrmulti1').innerHTML = clickMulti;
  document.getElementById('nbrmulti2').innerHTML = clickMulti2;
  document.getElementById('nbrmulti3').innerHTML = clickMulti3;
  document.getElementById('nbrautoclick1').innerHTML = clickAuto;
  document.getElementById('nbrautoclick2').innerHTML = clickAuto2;
  document.getElementById('nbrautoclick3').innerHTML = clickAuto3;
  document.getElementById('points').innerHTML = points;
  document.title = score + ' - Oiseaux-clicker';
  enableMulti();
  enableMulti2();
  enableMulti3();
  enableAutoClick();
  enableAutoClick2();
  enableAutoClick3();
  enableBonus();
}


//Fonction pour afficher le score
function increaseScore(){
  score = score + clickValue;
  update();
}
//Fonction pour afficher le nombre de points (autoclickers)
function pointsAuto(){
  points = clickAuto + clickAuto2*2 + clickAuto3*3;
  update();
}
//Fonction pour déclencher le son au clic
function soundPlay() { 
  let audio = new Audio('assets/son/moineau.mp3');
  audio.play();
}
function soundPlay2() { 
  let audio = new Audio('assets/son/pinson.mp3');
   audio.play();
  }
function soundPlay3() { 
   let audio = new Audio('assets/son/coucou.mp3');
    audio.play();
    }
function soundPlay4() { 
    let audio = new Audio('assets/son/pigeon.mp3');
     audio.play();
       }
function soundPlay5() { 
    let audio = new Audio('assets/son/hirondelle.mp3');
         audio.play();
           }
function soundPlay6() { 
  let audio = new Audio('assets/son/tourterelle.mp3');
      audio.play();
        }
  function soundPlay7() { 
    let audio = new Audio('assets/son/mesange.mp3');
        audio.play();
          }
  function soundPlay8() {
    let audio = new Audio('assets/son/merle.mp3');
        audio.play();
  }
moineau.onclick = function(){
   increaseScore();
   soundPlay();
}
// Fonction pour recharger la page
function reset(){
    location.reload();
    }

// Fonctions multiplicatrices
function increaseMultiplier() {
	clickValue *= multiplier;
	}

function increaseMultiplier2() {
  clickValue *= multi2;
}

function increaseMultiplier3() {
  clickValue *= multi3;
}

// Fonction bonus //
function increaseonus() {
  clickValue *= bonus;
}
// Fonction achat multiplicateurs //
function buyMultiplier() {
  if(score >= multiplierCost) {
    score = score - multiplierCost;
  }
}

function buyMulti2() {
  if(score >= multi2Cost) {
    score = score - multi2Cost;
  }
}
function buyMulti3() {
  if(score >= multi3Cost) {
    score = score - multi3Cost;
  }
}
// Fonction achat bonus //
function buyBonus() {
  if(score >= bonusCost) {
    score = score - bonusCost;
}
}

// Fonction activation bouttons multiplicateurs//
function enableMulti() {
  if(score >= multiplierCost){
    pinson.disabled = false;
    }
  else{
    pinson.disabled = true;
  }
}
function enableMulti2() {
  if(score >= multi2Cost){
    coucou.disabled = false;
    }
  else{
    coucou.disabled = true;
  }
}

function enableMulti3() {
  if(score >= multi3Cost){
    pigeon.disabled = false;
    }
  else{
    pigeon.disabled = true;
  }
}
  // Fonction activation bouttons autoclickers//
function enableAutoClick() {
  if(score >= autoClickCost){
    hirondelle.disabled = false;
    }
  else{
    hirondelle.disabled = true;
  }
}
function enableAutoClick2() {
  if(score >= autoClickCost2){
    tourterelle.disabled = false;
    }
  else{
    tourterelle.disabled = true;
  }
}
function enableAutoClick3() {
  if(score >= autoClickCost3){
    mesange.disabled = false;
    }
  else{
    mesange.disabled = true;
  }
}
//Function activation boutton bonus //
function enableBonus() {
  if(score >= bonusCost) {
    bonus.disabled = false;
  }
  else {
    bonus.disabled = true;
  }
}
pinson.onclick=function(){
  if(score >= multiplierCost) {
    increaseMultiplier();
    buyMultiplier();
    ajoutMulti();
    update();
    soundPlay2();
 }
}
coucou.onclick=function() {
  if(score >= multi2Cost) {
    increaseMultiplier2();
    buyMulti2();
    ajoutMulti2();
    soundPlay3();
    update();
  } else {
  coucou.disable = true;
 }
}

pigeon.onclick=function() {
  if(score >= multi3Cost) {
    increaseMultiplier3();
    buyMulti3();
    ajoutMulti3();
    soundPlay4();
    update();
  } else {
    pigeon.disable = true;
  }
}

function timer() {
  score = score + autoClick;
  update();
}
// Fonction achat d'autoclickers //
function buyAutoClick() {
  if (score >= autoClickCost) {
    score = score - autoClickCost;
    autoClick = autoClick + 1;
  }
  update();
}

function buyAutoClick2() {
  if (score >= autoClickCost2) {
    score = score - autoClickCost;
    autoClick = autoClick + 2;
  }
  update();
}

function buyAutoClick3() {
  if (score >= autoClickCost3) {
    score = score - autoClickCost;
    autoClick = autoClick + 3;
  }
  update();
}
    setInterval(function(){
    timer();
  }, 1000);
  // Indication visuelle du nombre d'achats //
  function ajoutMulti(){
    clickMulti = clickMulti + 1;
    update();
  }

  function ajoutMulti2(){
    clickMulti2 = clickMulti2 + 1;
    update();
  }

  function ajoutMulti3(){
    clickMulti3 = clickMulti3 + 1;
    update();
  }
  function ajoutAuto(){
    clickAuto = clickAuto + 1;
    update();
  }

  function ajoutAuto2(){
    clickAuto2 = clickAuto2 + 1;
    update();
  }

  function ajoutAuto3(){
    clickAuto3 = clickAuto3 + 1;
    update();
  }

hirondelle.onclick = function() {
  buyAutoClick();
  ajoutAuto();
  pointsAuto();
  soundPlay5();
  update();
}
tourterelle.onclick = function() {
  buyAutoClick2();
  ajoutAuto2();
  pointsAuto();
  soundPlay6();
  update();
}
mesange.onclick = function() {
  buyAutoClick3();
  ajoutAuto3();
  pointsAuto();
  soundPlay7();
  update();
}

bonus.onclick = function() {
  if(score >= bonusCost) {
    buyBonus();
    soundPlay8();
    update();
  }
}

// Bonus
// 1 replace 'bonus' par '30'
// 2 parseint 30
// 3 décompte 